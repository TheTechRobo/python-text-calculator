��    �      l  �   �      �     �     �  <  �     �  D     &   L     s     �  "   �     �  "   �  K   �  �   G     E  7   [     �  Q   �  K        N  )   e  
   �  Q  �  \   �     I     e     }     �  7   �     �     �               !     @  �   R      �        	  Z     *   u  &   �  (   �     �           1     H     Z     s  "   �  &   �  )   �  +        0     B     T  &   s     �     �     �  )   �  D   �     :  A   L  ~   �  3     �   A  Z   �  
   +     6     E     X     t     �  "   �     �     �     �     �        O      L   j      �   7   �       !  $   )!  !   N!  #   p!  %   �!  !   �!  %   �!  5   "  (   8"  5   a"      �"  *   �"  &   �"  &   
#  &   1#  %   X#  ,   ~#  .   �#  &   �#  '   $  s   )$     �$     �$  )   �$  "   �$  +   %      I%  $   j%  (   �%  (   �%  (   �%  &   
&  "   1&  G   T&  )   �&  *   �&     �&  )   '  +   7'  %   c'  )   �'     �'  k   �'  6   >(  %   u(  A   �(  "   �(      )  &    )  t   G)     �)     �)  $   �)  '   *  �  9*  
   �+  �  �+  �  �-     �/  (   �/  /   �/     0     0  9   50     o0  *   �0  Z   �0  r  1     �2  8   �2  +   �2  U   3  Z   Z3     �3  ?   �3     4  w  %4  r   �6     7     !7     :7     I7  H   ]7  *   �7     �7     �7     8  $   8     >8  �   W8  .  '9     V:     W;  `   t;  &   �;  (   �;  +   %<     Q<     d<     w<     �<  !   �<  (   �<  &   �<  /   =  7   I=  0   �=     �=     �=  )   �=  "   >     '>     <>     N>     c>  B   |>     �>  A   �>  �   ?  7   �?  �   �?  Z   w@     �@     �@     �@      A     )A     <A  %   YA     A     �A     �A     �A     �A  I   �A  Z   <B     �B  6   �B     �B     C     C     -C     DC     \C     pC  -   �C     �C  -   �C     D     D     7D     SD     kD     �D     �D     �D     �D  (   �D  x   E     �E      �E  *   �E     �E     F     .F     @F     VF  +   oF  .   �F     �F     �F  H   �F  !   @G  *   bG  *   �G  *   �G     �G  +   �G  #   &H  .   JH  �   yH  8   �H  $   4I  C   YI  %   �I  %   �I  %   �I  I   J     YJ      rJ     �J     �J     x   V             9   a               G   o       8   &   �         "   m      5           E   #   S   0         B             O   D      T   '              {   6          b   W   
                         :         r   t       )   w       k   .       /          -   A   z   �       d   Q   ?   ^              j   3   �   f       h   <              c   �   �       �   }       @   n   7   U   C       q   u       1   i       Z   (       I   H       P       �              X       ~   �   	      K   !      J   L       �   4   N       ]               R   2       g          �       =   p   l   y   s   _       ;   Y       |   \      �   e           M   %   $   *   `   F                  +          ,   [       v      >              ERROR! 
            I don't understand your request. Here are the currently supported calculations:
            * or x; / or div; -, min, or sub; + or add; % or mod (modulo); sq or [] (square); ar or # (area); vol (volume); {} (cube); power (exponents/power); root (roots); = (equals); fib (fibonacci) log (logarithm); mem (memory); percent (calculate percentage); interest (interest calculator); temperature (convert Celsius to Farenheit etc); and base (convert number system). Sorry for the inconvenience
             
     Current list of commands: multiplication (*), division (/), addition (+), square (sq), subtraction (-), modulo (%), area (#), volume (vol), cube ({}), cube twice ({2}), exponents (power), root (root), equals (=), logarithm (log), memory (mem), interest calculator (interest), fibonacci sequence (fib), percentage (percent), convert temperature (temperature), and convert number systems (base). Type quit to quit.
     Bugs? Head on over to https://github.com/thetechrobo/support/
     To contribute: go to https://github.com/thetechrobo/python-text-calculator/
      
Error! 
Note that you CAN type `quit' instead of pressing the interrupt key 
Please enter the number to be saved:  
That equals... 
That equals...
%s 
Type the first number (greater):  
Type the number to be cubed:  
Why ^D? Why not just type `quit'? 1 - Calculate "What is x% of y?"
2 - Convert a number to percentage.
Type:  1 - Ontario Sales Tax
2 - Quebec Sales Tax
3 - Yukon, Northwest Territories, Nunavut, and Alberta Sales Tax
4 - BC / Manitoba Sales Tax
5 - New Brunswick / Nova Scotia / Newfoundland / PEI Sales Tax
6 - Saskatchewan Sales Tax
7 - Custom Tax
Choose one:  5 - Empty Custom Slot =42 -- the answer to life, the universe, and everything After tax, the price is: 
%s An unknown error occured. Please file an Issue at github.com/thetechrobo/support. Currently I don't support the root you chose. Hopefully this will change :) Do not divide by zero! Do you really need a calculator for this? Exponent?  How many units of time would you like to calculate? 
Essentially, one unit of time could be one month, or one decade. It all depends on what you typed in the rate of interest question: it could be per year, per decade...we didn't ask.
It was up to you to type the correct amount in the rate question.
We have no idea what the rate represented: it could have been that rate per century for all we know.
This calculator wasn't programmed with the ability to track time.
So, with that out of the way, type the amount we should multiply the interest by (aka the amount of units of time).
Type it:  I can't access the file func.py. This file is necessary for proper function of the Software. I was too lazy to change 7. Invalid input (code 1)
 Loading...............
 Looks like you exited. Now enter the tax percentage without the percent sign:  Number to be rooted? Number to be rooted?  Number:  Number?  OK, enter the original price:  OK. Going back... OPTIONS:
    1 - Farenheit to Celsius
    2 - Celsius to Farenheit
    3 - Farenheit to Kelvin
    4 - Celsius to Kelvin
    5 - Kelvin to Celsius
    6 - Kelvin to Farenheit
    Type:  Options:
1 - Cube
2 - Cuboid
3 - Cylinder
4 - Hollow cylinder
5 - Cone
6 - Sphere
8 - Hollow sphere
9 - Triangular prism
10 - Pentagonal prism
11 - Hexagonal prism
12 - Square-based pyramid
13 - Triangular pyramid
14 - Pentagon-based pyramid
15 - Hexagon-based pyramid Options:
1 - Equilateral triangle
2 - Right angle triangle
3 - Acute triangle
4 - Obtuse triangle
5 - Square
6 - Rectangle
8 - Parallelogram
9 - Rhombus
10 - Trapezium
11 - Circle
12 - Semicircle
13 - Circular sector
14 - Ring
15 - Ellipse Original number? Please do not run any of these files directly. They don't do anything useful on their own. Please do not type in a zero as the whole. Please enter the CELSIUS temperature:  Please enter the FARENHEIT temperature:  Please enter the first number:  Please enter the second number:  Please type an integer Please type one:  Press Control-C to stop. Press any key to continue... Sorry, that was not an option. >:) Square root or cube root?(square/cube) Starting fibonacci sequence. Please wait. Thanks for using Palc's FIBONACCI function! That equals...
%s That equals.....
 That feature was discontinued. That was an invalid answer. Try again. The answer is: 
%s The area is:  The area is: %s The logarithm you typed is not available. The second number entered is greater than the first number (code 2)
 The volume is: %s There may have been details before the word `ERROR!'. Check that. There was an error reading the file. Did you save the number by using the save function? Did you accidentally rename the file? There was an unknown issue dividing your Numbers... This is the memory function.
It will save a number into a file that can be used later with Palc... Or you can just read it with a text editor. This is the remember function.
It will read a number that was previously stored in a file. Try again. Type a name... Type in a number:  Type in the number to ord:  Type something! Type the original number:  Type the second number (smaller):  Unknown Error! Using base 10 Using natural logarithm Welcome to Palc! What Percentage?  What base would you like to use? 
Currentlysupported: 10 (base 10), e (natural) What calculation do you wish to do? (Type `?' for a list of commands)
Type:  What is the PERCENTAGE?  What is the angle of the circular sector *in degrees*?  What is the height of the cone?  What is the height of the cylinder?  What is the height of the prism?  What is the height of the pyramid?  What is the height of the rectangle?  What is the height of the shape?  What is the height of the trapezium?  What is the length of the 1st set of parallel sides?  What is the length of the 2nd diagonal?  What is the length of the 2nd set of parallel sides?  What is the length of the base?  What is the length of the first diagonal?  What is the length of the first side?  What is the length of the major axis?  What is the length of the minor axis?  What is the length of the rectangle?  What is the length of the side of the base?  What is the length of the side of the square?  What is the length of the third side?  What is the number that would be 100%?  What is the number that you want to convert to percentage (e.g. this number out of the number that would be 100%)?  What is the number?  What is the original number?  What is the original price (before tax)?  What is the radius of the circle?  What is the radius of the circular sector?  What is the radius of the cone?  What is the radius of the cylinder?  What is the radius of the hollow space?  What is the radius of the inner circle?  What is the radius of the outer circle?  What is the radius of the semicircle?  What is the radius of the sphere?  What is the rate of interest in percentage (without the percent sign)?  What length is the base of the triangle?  What length is the breadth of the cuboid?  What length is the cuboid?  What length is the height of the cuboid?  What length is the height of the triangle?  What length is the side of the cube?  What length is the side of the triangle?  What slot number did you use?  What slot would you like to use? (Hint: you can use any integer you want as long as you remember it)
Type:  Which type is the Number (ord, binary, octo, or hex):  Would you like to do it again (Y/n)?  Would you like to set the memory or recall? (set / recall)
Type:  You did not type an answer.
Abort. You did not type answer. Abort. You didn't type a valid answer. Abort. You typed in an invalid integer or float. Or maybe the program needs debugging. Either way, it's a pretty big error. an error occured. what is the ORIGINAL NUMBER?  what is the height of the cylinder?  what is the length of the second side?  Project-Id-Version: Palc
POT-Creation-Date: 2020-05-23 11:54-0400
PO-Revision-Date: 2020-05-23 11:55-0400
Last-Translator: ittussarom
Language-Team: 
Language: fr_CA
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.11
X-Poedit-Basepath: ../../..
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: _
X-Poedit-SearchPath-0: .
 Erreure ! 
            Je ne sais pas quoi vous voulez faire. Ça c'est une liste de les calcules supportes :
            * ou x pour la multiplication; -, min, ou sub pour la soustraction, + ou add pour l'addition; sq ou [] pour carre; ar ou # pour l'aire; vol pour le volume; {} pour le cube; ex pour les exponents; root pour les racines; log pour le logarithme; fib pour l'équation fibonacci; mem pour la mémoire; et base pour
            convertir les numéros a une autre système. Désolé : /
             
    Liste des calculs : multiplication (*), division (/), addition (+), carre (sq), soustraction (-), modulo (%), aire (#), volume (vol), cube ({}), exponents (ex), racine (root), egale (=), logarithme (log), memoire (mem), et convertir une nombre au une systeme different (base). Tappez exit pour exiter.
    Erreurs ? Allez a https://github.com/thetechrobo/support
    Si tu veux contribuer : https://github.com/thetechrobo/python-text-calculator
      
Une erreur est survenue. 
Tu peux tappez `quit' plutot que ^C...  
Quelle numero est-ce-que tu veux etre sauve :  
La reponse est... 
La reponse est...
%s 
Tappez le nombre premier (plus grand que la deuxieme) :  
Tappez le nombre à cuber :  
Pourquoi ^D ? Pourquoi pas juste `quit' ? 1 - Calcule << Quelle est x % de y ? >>
2 - Convertir une nombre au pourcentage.
Tappez :  1 - Taxe de vente de l'Ontario 2 - Taxe de vente du Québec 3 - Taxe de vente du Yukon, des Territoires du Nord-Ouest, du Nunavut et de l'Alberta 4 - Taxe de vente de la Colombie-Britannique / Manitoba 5 - Taxe de vente du Nouveau-Brunswick / Nouvelle-Écosse / Terre-Neuve / Î.-P.-É. 
6 - Taxe de vente de la Saskatchewan 
7 - Taxe personnalisée 
Choisissez-en une:  5 - Slot customisè vide =42 -- la reponse au vie, l'universe, et tous les choses Après qu'on ajoute taxe, le prix est : 
%s Une erreure a se passé. Veuillez mettre une Issue à github.com/thetechrobo/support. Pour le moment je ne supporte pas la racine que vous voulez. J'espere que ca va changer :) Ne divisez pas par zéro ! Est-ce-que tu as vraiment besoin d'une calculatrice pour ça ?  Tappez l'exponent :  Combien des unités te temps est-ce-que tu veux calculer ? 
Une unité de temps peut être une mois, ou 10 ans. Il dépend tout sur quoi tu as tapez dans le taux de l'intérêt : ce pouvait représenter une mois, une année...on n'a pas demandé.
C'était pour vous pour tapez le réponse correcte. 
On ne sais pas du tous quoi le taux a représenté : ce pouvait être cette taux par siècle pour tous qu'on savais.
Cette calculatrice n'étais pas donne l'abilité pour savoir le temps.
Donc, avec ça hors du chemin, tappe le montant que on dois multiplier l'intérêt par (aka le montant de unités de temps).
Tappe le numero :  Je ne peux pas trouver le fichier func.py. Cette fichier est necessaire pour la fonction propre de cette logiciel. 7 n'existe pas ! Nombre invalid (code 1)
 Chargement...
 Tu as quitter Palc. Quel est le montant de taxe en pourcentage (sans le signe pourcentage)?  Quelle est la nombre pour << raciner >> ?  Quelle est la nombre ?  Quelle est le nombre ?  Quelle est le nombre ?  D'accord, tappez le prix original :  D'accord. Je retourne... OPTIONS : 
    1 - Fahrenheit à Celsius
    2 - Celsius à Fahrenheit
    3 - Fahrenheit à Kelvin
    4 - Celsius à Kelvin
    5 - Kelvin à Celsius
    6 - Kelvin à Fahrenheit
    Tappez une réponse :  Options:
1 - Cube
2 - Parallélépipède
3 - Cylindre
4 - Cylindre creux
5 - Cone
6 - Sphere
8 - Sphere creux
9 - Prisme triangulaire
10 - Prisme pentagonal
11 - Prisme hexagonal
12 - Pyramide a base carre
13 - Pyramide a base triangulaire
14 - Pyramide a base pentagonal
15 - Pyramide a base hexagonal Les options :
1 - Triangle equilateral
2 - Triangle avec une angle droite
3 - Triangle acute
4 - Triangle obtus
5 - Carre
6 - Rectangle
8 - Parallelogramme
9 - Losange
10 - Trapeze
11 - Cercle
12 - Demi-cercle
13 - Sectore circulaire
14 - Ring
15 - Ellipse Tappez le nombre original :  Veuillez n'exécuter aucun de ces fichiers directement. Ils ne font rien d'utile par eux-mêmes. N'écrivez pas une zero pour l'entier. Veuillez entrer la température CELSIUS: Veuillez entrer la température FARENHEIT:  Premiere numero ?  Deuxieme numero ?  SVP tappez une integer SVP tappez une option :  Appuyez sur Ctrl-C pour arrêter. Appuyer sur une touche pour continuer… Desole, ce n'etais pas une option. >:) Racine carré ou cube ? (Square/cube)
Tappez :  Je commence le séquence Fibonacci. Veuillez attendre.  Merci d'utiliser le Fibonacci fonction de Palc ! La reponse est...
%s La reponse est...
 Cette fonctionnalité a été supprimée. Reponse invalide, essaie encore !  La reponse est : 
%s La reponse est :  La réponse est : %s La reponse est invalide. Le nombre deuxieme est plus grand que la premiere nombre (code 2)
 La reponse est : %s Peut-être des détails du erreur sont avant le mot `ERREURE ! '. Il y etait une erreur pour lire le fichier. Est-ce-que tu as sauve le numero ? est-ce-que tu as par accident renommer le fichier ?  Il y avait un problème inconnu divisant vos numéros.  Ca c'est la fonction memoire. 
Ca va sauver une numero en une fichier que peux etre utiliser dans la futur avec Palc... Ou tu peux juste lire avec une editeur texte. Ca c'est le fonction lire memoire.
Ca va lire une numero qui a ete sauve dans une fichier. Essaie encore. Tappez une nom ... Tappez le nombre :  Tappez le nombre pour 'ord'er :  Tappez une chose ! Tappez le nombre original :  Tappez le 2eme numero (plus petit) :  Une erreure a survenue. utilisant base 10  Utilisant la logarithme naturel Bienvenue au Palc ! Quel pourcentage ?  Quelle systeme est-ce-que tu veux utiliser ? 
10 (decimal) ou e (natural) Quelle calculation est-ce-que tu veux faire? (Dit `?' pour une liste de calcules)
Tappe :  Qu'est-ce que le POURCENTAGE ?  Quel est l'angle du secteur circulaire * en degrés *? Hauteur du cone ?  Hauteur du cylindre ?  Hauteur du prisme ?  Hauteur du pyramide ?  Hauteur du rectangle ?  Hauteur du forme ?  Hauteur du trapeze ?  Longeur de la 1ere set des cotes parallels ?  Longeur de la 2eme diagonal ?  Longeur de la 2eme set des cotes parallels ?  Longeur de la base ?  Longeur du deuxieme diagonal ?  longeur du premiere cote ?  Longeur d'axis major ?  Longeur de l'axis minor ?  Longeur du rectangle ?  Longeur de la cote du base ?  Longeur du cote du carre ?  Longeur du troisieme cote ?  Quelle est la nombre qui serait 100 % ?  Quel est le nombre que vous souhaitez convertir en pourcentage (par exemple, ce nombre sur le nombre qui serait 100%) ?  Quelle est la nombre ?  Quelle est le nombre original ?  Quelle est le prix original (sans taxe) ?  Radius du cercle ?  Radius du sector circulaire ?  Radius du cone ?  Radius du cylindre ?  Radius d'espace creux ?  C'est quoi le radius de la cercle inneur ?  C'est quoi le radius de la cercle exterieur ?  Radius du demi-cercle ?  Radius du sphere ?  Quel est le taux d'intérêt en pourcentage (sans le signe pourcentage)? Longeur de la base du triangle ?  Largeur du parallélépipède rectangle ?  Longeur du parallélépipède rectangle ?  Hauteur du parallélépipède rectangle ?  Hauteur du triangle ?  Quelle est le longeur de la cote du cube ?  Longeur du côté de la triangle ?  Quelle numero slot est-ce-que tu as utilise ?  Quelle slot memoire est-ce-que tu veux utiliser ? (Indice : tu peux utiliser n'importe quelle nombre si tu le souvient)
Tappez :  Quelle type est le numero (ord, binaire, oct, ou hec) :  Voulez-vous faire ça encore (Y/n)?  Souhaitez-vous setter la mémoire ou rappeler? (set / recall)
Type: Tu n'a pas tappez une reponse valide. Tu n'a pas tappez une reponse valide. Tu n'a pas tappez une reponse valide. Erreure: Tu as tappez une nombre invalide, ou le logiciel a une probleme. Une erreur est survenue. Quelle est le nombre original ?  hauteur du cylindre ?  longeur du deuxieme cote ?  